package com.example.jaro.beaconstest;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.bluetooth.BluetoothAdapter;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import org.piwik.PiwikException;
import org.piwik.SimplePiwikTracker;

import java.net.URL;
import java.util.List;
import java.util.UUID;

public class BeaconDistance extends Activity {
    private static final String TAG = BeaconDistance.class.getSimpleName();

    private static final String PIWIK_API_BASE = "http://beacons.testing.piwik.pro";
    /*  All beacons has the same UUID. It can be changed by using SDK, but after that beacons
        won't be visible in Estimote's app anymore. As it might be useful (some beacons settings
        can be easily changed by using it) the assumption is to use Major ID as beacon identifier.
        Proximity UUID is used only for ranging the beacons. */
    private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("regionId", ESTIMOTE_PROXIMITY_UUID, null, null);

    private TextView textView;
    private BeaconManager beaconManager;
    static public ProgressBar progress_bar;
    static public TextView statusTextPiwik;
    static public Button button_send_code;
    private TimeTracker timeTracker = new TimeTracker();
    private EditText pseudo_qr_code;

    public static Activity root_view_activity;

    public static SimplePiwikTracker tracker;
    public static double MAX_DISTANCE = 0.6;   // in meters. Accuracy of distance is rather low, it can fluctuate between 0.5 and 2 meters  TODO: build UI for setting this

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ######## Set view ##########
        setContentView(R.layout.activity_beacon_distance);
        textView = (TextView) findViewById(R.id.console_log);
        textView.setTextSize(20);

        // hack for set timeout
        root_view_activity = this;

        progress_bar = (ProgressBar) findViewById(R.id.progressbar_send_code);
        pseudo_qr_code = (EditText) findViewById(R.id.piwik_code);
        statusTextPiwik = (TextView) findViewById(R.id.status_text_piwik);

        // Listener for send code button
        button_send_code = (Button) findViewById(R.id.button_piwik_send_code);
        button_send_code.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String qr_code = pseudo_qr_code.getText().toString();
                pseudo_qr_code.setText("");
                button_send_code.setEnabled(false);

                statusTextPiwik.setVisibility(View.INVISIBLE);
                statusTextPiwik.setText("");

                new GoalRequestTask().execute(qr_code);
            }
        });

        // ######## Init piwik ##########
        try {
            this.initPiwik();
        } catch (Exception e){
            textView.setText("Cannot into piwik: " + e.toString());
            return;
        }

        // ######## Beacon Manager ###########
        if (!"goldfish".equals(Build.HARDWARE)) {
            beaconManager = new BeaconManager(this);
            beaconManager.setRangingListener(new BeaconManager.RangingListener() {
                @Override public void onBeaconsDiscovered(Region region, final List<Beacon> rangedBeacons) {
                    String message = "Ranged beacons num: " + rangedBeacons.size();

                    for (Beacon rangedBeacon : rangedBeacons) {
                        Integer major = rangedBeacon.getMajor();
                        message += "\n\nTracked with beacon identified by Major: " + major;
                        timeTracker.track(rangedBeacon);
                    }

                    for (Event event: timeTracker.firedEvents()){
                        try {
                            sendToPiwik(event);
                            message += "\n\nSent to piwik " + event.toString();
                        } catch (PiwikException e){
                            message += "\n\nThere is a problem with sending request..";
                        }
                    }

                    textView.setText(message);
                }
            });
        } else {
            new RequestTestCvars().execute("DEV");
        }
    }

    private void sendToPiwik(Event event) throws PiwikException{
        new RequestTask().execute(event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (beaconManager != null) {
            beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                @Override public void onServiceReady() {
                    try {
                        beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                        Log.d(TAG, "Starting ranging");
                        textView.setText("Ranging");
                    } catch (RemoteException e) {
                        Log.e(TAG, "Cannot start ranging", e);
                    }
                }
            });
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (beaconManager != null) {
            try {
                beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
            } catch (RemoteException e) {
                Log.e(TAG, "Cannot stop but it does not matter now", e);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (beaconManager != null) {
            int i = 0;

            for (Event event: timeTracker.purgeAllEvents()){
                try {
                    i += 1;
                    sendToPiwik(event);
                    Log.i("on destroy", "ok");
                } catch (PiwikException e){
                    Log.e("on destroy", e.toString());
                }
            }

            try {
                Thread.sleep(i * 1000);
            }catch (Exception e){
                Log.e("d", e.toString());
            }

            Log.i("on destroy", "done");

            beaconManager.disconnect();
        }
        super.onDestroy();
    }

    private String getUserId(){
        // calculate device UUID using bluetooth MAC address
        try {
            //BluetoothAdapter blAdapter = BluetoothAdapter.getDefaultAdapter();
            //String blMAC = blAdapter.getAddress();
            UUID deviceID =  UUID.randomUUID(); //(blMAC.hashCode(), blMAC.hashCode() *  37);
            Log.i(TAG, "Device ID " + deviceID);
            return SimplePiwikTracker.md5(deviceID.toString()).substring(0, 16);
        } catch (Exception e){
            // hack for emulator
            return "1234567812345678";
        }

    }

    private void initPiwik() throws PiwikException{
        BeaconDistance.tracker = new SimplePiwikTracker(PIWIK_API_BASE);
        BeaconDistance.tracker.setVisitorId(this.getUserId());
        BeaconDistance.tracker.setIdSite(3);
        UUID random =  UUID.randomUUID();
        BeaconDistance.tracker.setUserAgent("Mozilla/5.0 (Linux; U; Android 2.3.5; zh-cn; " +
                "NexusOsiemnascie Build/GRJ90) AppleWebKit/533.1 (KHTML, like Gecko) " +
                "Version/4.0 Mobile Safari/533.1" + random.toString().substring(0, 5));
        BeaconDistance.tracker.setIp("127.0.0." + random.toString().hashCode() % 256);
        BeaconDistance.tracker.setTokenAuth("1e7996408cbab0c443d71931f08e2fee");
    }

    private String[] getLatLon() {
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location != null) {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            return new String[]{"" + latitude, "" + longitude};
        }

        return new String[]{"", ""};
    }
}
