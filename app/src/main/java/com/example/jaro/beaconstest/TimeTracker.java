package com.example.jaro.beaconstest;

import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alex on 2014-08-01.
 */
public class TimeTracker {
    
    private HashMap<Beacon, Event> data = new HashMap<Beacon, Event>();

    public List<Event> purgeAllEvents(){
        List<Event> events = new ArrayList<Event>();

        for(Event event: data.values()){
            if(event.ticks > 0) {
                events.add(event);
            }
        }
        this.data.clear();
        return events;
    }
    public List<Event> firedEvents() {
       List<Event> events = new ArrayList<Event>();
       List<Beacon> beacons = new ArrayList<Beacon>();
       for(Map.Entry<Beacon, Event> entry: data.entrySet()){
           Event event = entry.getValue();
           if(event.shouldBeFire()){
               events.add(event);
               beacons.add(entry.getKey());
           }
       }

       for(Beacon beacon: beacons){
           data.remove(beacon);
       }

       return events;
    }

    private double getDistance(Beacon beacon) {
        double distance = Utils.computeAccuracy(beacon);
        Log.i("time tracker", String.format("Distance of %d: %.2f", beacon.getMajor(), distance));
        return distance;
    }

    public void track(Beacon rangedBeacon) {
        double distance = this.getDistance(rangedBeacon);

        if(this.data.containsKey(rangedBeacon)){
            Event event = this.data.get(rangedBeacon);
            event.tick(distance);
        }else{
            Event event = new Event(rangedBeacon);
            event.tick(distance);
            this.data.put(rangedBeacon, event);
        }

    }
}
