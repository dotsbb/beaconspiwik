package com.example.jaro.beaconstest;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import com.estimote.sdk.Beacon;

import org.piwik.PiwikException;

import java.net.URL;


public class GoalRequestTask extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
        String code = params[0].trim();

        if (code.equals("") || code.equals("0")) {
            code = "1";
        }

        Long int_code = Long.parseLong(code);

        if ((int_code % 1024) == 0) {

            BeaconDistance.root_view_activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    BeaconDistance.progress_bar.setVisibility(View.VISIBLE);
                }
            });

            try {
                BeaconDistance.tracker.clearCustomVariables();
                BeaconDistance.tracker.setPageCustomVariable("goal", "reached");
                BeaconDistance.tracker.setPageUrl("http://beacons.dev/goal_reached.html");
                URL url = BeaconDistance.tracker.getGoalTrackURL(
                        "1",
                        Long.toString(int_code / 1024)
                );
                BeaconDistance.tracker.sendRequest(url);
            } catch (PiwikException e) {
                Log.e("LongOperation", "piwik fail", e);
                return "Error.";
            }

            return "Congratulations!";
        } else {
            return "Wrong code! Try again.";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        BeaconDistance.progress_bar.setVisibility(View.INVISIBLE);

        BeaconDistance.statusTextPiwik.setVisibility(View.VISIBLE);
        BeaconDistance.statusTextPiwik.setText(result);

        BeaconDistance.button_send_code.setEnabled(true);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                BeaconDistance.root_view_activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        BeaconDistance.statusTextPiwik.setVisibility(View.INVISIBLE);
                        BeaconDistance.statusTextPiwik.setText("");
                    }
                });
            }
        }, 3000);

        Log.i("sent track event", result);
    }
}
