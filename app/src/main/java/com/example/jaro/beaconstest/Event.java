package com.example.jaro.beaconstest;

import com.estimote.sdk.Beacon;

import java.text.DecimalFormat;
import java.util.LinkedList;

public class Event {

    private final int DEFAULT_PAUSE = 4; // seconds
    public Beacon beacon;
    private LinkedList<Double> distance = new LinkedList<Double>();
    public long ticks = 0;
    public long blankTicks = 0;
    private DecimalFormat df = new DecimalFormat("#.#");

    public Event(Beacon beacon){
        this.beacon = beacon;
    }

    public String toString(){
        return "beacon:" + beacon.getName() +
                " Duration: " + this.getDuration() +
                " Avg distance: " + this.getAvgDistance();
    }

    public String getDuration() {
        return Long.toString(this.ticks);
    }

    public String getAvgDistance() {
        Double sum = (double) 0;
        for(Double dis : distance){
            sum += dis;
        }
        return df.format(sum/distance.size());
    }

    public void addDistance(double distance) {
        this.distance.push(distance);
    }

    public void tick(double distance) {
        if(distance <= BeaconDistance.MAX_DISTANCE) {
            this.addDistance(distance);
            this.ticks += 1;
            blankTicks = 0;
        }else{
            blankTicks += 1;
        }
    }

    public boolean shouldBeFire(){
        // we saw him lately, fire event if hi stays here for at least 5 second
        // and then disappears for at least 4 seconds
        return ticks > 0 && blankTicks > DEFAULT_PAUSE;
    }
}
