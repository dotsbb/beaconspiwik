package com.example.jaro.beaconstest;

import android.os.AsyncTask;
import android.util.Log;

import com.estimote.sdk.Beacon;

import org.piwik.PiwikException;

import java.net.URL;
import java.util.Random;

/**
 * Created by tomek on 02.08.14.
 */
public class RequestTestCvars extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {

        int min = 1;
        int max = 5;

        Random r = new Random();

        int time_of_stay = r.nextInt(max - min + 5) + min;
        int distance = r.nextInt(max - min + 1) + min;

        try {
            String _dev = params[0];
            Integer major = 666;
            BeaconDistance.tracker.setPageUrl("/" + Integer.toString(major) + ".html");
            BeaconDistance.tracker.clearCustomVariables();
            BeaconDistance.tracker.setPageCustomVariable("Category", "Beacon");
            BeaconDistance.tracker.setPageCustomVariable("BeaconId", "666");
            BeaconDistance.tracker.setPageCustomVariable("AppId", "1");
            BeaconDistance.tracker.setPageCustomVariable("BeaconTimeOfStay", Integer.toString(time_of_stay));
            BeaconDistance.tracker.setPageCustomVariable("Distance", Integer.toString(distance));
            URL url = BeaconDistance.tracker.getPageTrackURL(Integer.toString(major));
            BeaconDistance.tracker.sendRequest(url);
            Log.w("request sent", _dev);
        } catch (PiwikException e) {
            Log.e("LongOperation", "piwik fail", e);
            return "error";
        }
        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
        Log.i("sent dev test", result);
    }
}
