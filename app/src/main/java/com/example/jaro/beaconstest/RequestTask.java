package com.example.jaro.beaconstest;

import android.os.AsyncTask;
import android.util.Log;

import com.estimote.sdk.Beacon;

import org.piwik.PiwikException;

import java.net.URL;


public class RequestTask extends AsyncTask<Event, Void, String> {

    @Override
    protected String doInBackground(Event... params) {
        try {
            Event event = params[0];
            Beacon beacon = event.beacon;
            String duration = event.getDuration();
            String avgDistance = event.getAvgDistance();
            Integer major = beacon.getMajor();
            BeaconDistance.tracker.setPageUrl("http://beacons.dev/" + Integer.toString(major) + ".html");
            BeaconDistance.tracker.clearCustomVariables();
            BeaconDistance.tracker.setPageCustomVariable("Category", "Beacon");
            BeaconDistance.tracker.setPageCustomVariable("BeaconId", Integer.toString(major));
            BeaconDistance.tracker.setPageCustomVariable("AppId", "1");
            BeaconDistance.tracker.setPageCustomVariable("BeaconTimeOfStay", duration);
            BeaconDistance.tracker.setPageCustomVariable("Distance", avgDistance);
            URL url = BeaconDistance.tracker.getPageTrackURL(Integer.toString(major));
            BeaconDistance.tracker.sendRequest(url);
            Log.w("request sent", event.toString());
        } catch (PiwikException e) {
            Log.e("LongOperation", "piwik fail", e);
            return "error";
        }
        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {
        Log.i("sent track event", result);
    }
}
